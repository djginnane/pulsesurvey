﻿using System.Web;
using System.Web.Mvc;
using Umbraco.Core;
using Umbraco.Web.Mvc;
using Umbraco.Core.Models;
using Newtonsoft.Json;
using System.IO;
using System;
using System.Collections.Generic;
using System.Linq;



/// <summary>
/// Summary description for GamesSurfaceController
/// </summary>
public class EmployeeSurfaceController : SurfaceController
{


    [HttpPost]
    public ActionResult UploadEmployeeAccounts(KeyValue model)//,List<InsuredPerson> Insured_Persons)
    {
        string fileName = model.value;
        var member = EmployeeHelper.LoggedInMember;
        var db = ApplicationContext.Current.DatabaseContext.Database;
        var hub = DataHelpers.CurrentHub(member);
        if (!ModelState.IsValid)
        {
            ViewBag.Message = "Could not upload file, please ensure it is a csv file";
            return CurrentUmbracoPage();
        }
        var GameAccountList = new List<UserTemp>();
        for (int i = 0; i < Request.Files.Count; i++)
        {
            var file = Request.Files[i];

            if (file != null && file.ContentLength > 0)
            {
                fileName = Path.GetFileName(file.FileName);
                var extension = Path.GetExtension(fileName);
                var File_Id = Guid.NewGuid();




                var dirPath = Path.Combine(Server.MapPath("~/App_Data/TEMP/FileUploads/"));

                if (!Directory.Exists(dirPath))
                {
                    System.IO.Directory.CreateDirectory(dirPath);
                }
                fileName = File_Id.ToString();
                var path = dirPath + fileName + extension;

                file.SaveAs(path);
                GameAccountList = FileHelper.Main(path);

                FileHelper.AddUsers(GameAccountList);
                /********Tempuser *********/
                //   FileHelper.AddUsersTemp(GameAccountList);
                if (GameAccountList.Count() > 0)
                {
                    //TODO:Add correct hub
                    var UploadDetails = new UploadedCsv();//new UploadedCsv();

                    UploadDetails.Filename = fileName + extension;
                    UploadDetails.DateUPloaded = DateTime.UtcNow;
                    UploadDetails.UploadedBy = EmployeeHelper.LoggedInMember.Id;
                    UploadDetails.Original_Document_Name = file.FileName;
                    UploadDetails.LastLoaded = DateTime.UtcNow;
                    UploadDetails.StaffCount = GameAccountList.Count();
                    UploadDetails.hub = hub;

                    db.Insert(UploadDetails);


                }

            }
        }
        if (GameAccountList.Count() == 0)
        {

            TempData["tempvalErr"] = "Could not upload file, please ensure it is a csv file and that there is no invalid data in the columns";
        }
        else
        {
            /*** Uploading useing member system ******/
          //  EmployeeHelper.DeleteStaffLevels(hub);
         //   EmployeeHelper.CreateStaffLevels(GameAccountList, hub);
          


            /*****Uploading using userlite *******/
            // EmployeeHelper.CreateStaffLevels(GameAccountList, hub);

            TempData["customerval"] = "Employees Uploaded Successfully.";
        }

        return RedirectToCurrentUmbracoPage();
    }
    public EmployeeSurfaceController()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}