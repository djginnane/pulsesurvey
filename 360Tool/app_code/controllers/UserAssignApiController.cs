﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using umbraco;
using Umbraco.Core;
using Umbraco.Web.WebApi;


public class UserAssignApiController : UmbracoApiController
{
    [HttpGet]
    public HttpResponseMessage GetUserList()
    {
        // var suppliers = new List<Supplier>();
        //  var repo = new CashFlowRepository();
        var AssignedList = UserAssignHelper.GetAssigned();
        //  suppliers = repo.GetSupplierList("Popcorn");
        return Request.CreateResponse(HttpStatusCode.OK, AssignedList, new JsonMediaTypeFormatter());
    }

    [HttpGet]
    public HttpResponseMessage GetModels()
    {
        var uModels = new UserModels();
        uModels.UserAssigned = new AssignedSurveys();
        uModels.UserDet = new UserDetails();
        return Request.CreateResponse(HttpStatusCode.OK, uModels, new JsonMediaTypeFormatter());
    }


    /// <summary>
    /// Assigns all choosen user to complete the survey, but does not generate the survey
    /// </summary>
    /// <param name="AssignedList"></param>
    /// <returns></returns>
    [HttpPost]
    public HttpResponseMessage AssignUser( List<AssignedSurveys> AssignedList, string TemplateId= "2A7E3554-72D0-4528-A7D6-787F74A8605B")
    {
        var updatedAssigned = new List<AssignedSurveys>();
        var repo = new AssignedSurveyRepo();
        foreach (var AL in AssignedList)
        {
            if (AL.status==0)
            {
                //AL.status = 1;
                //AL.DateAssigned = DateTime.UtcNow;             
                //AL.TemplateId = new Guid (TemplateId);
                //AL.SurveyBatch = Guid.NewGuid();
               var newAssigned= UserAssignHelper.MapNewAssigned(AL, TemplateId);
                var newAssign = repo.AddAssigned(newAssigned);
                updatedAssigned.Add(newAssign);

            }
            else
            {
                updatedAssigned.Add(AL);
            }
        }
        return Request.CreateResponse(HttpStatusCode.OK, updatedAssigned, new JsonMediaTypeFormatter());
    }
}
