﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using umbraco;
using Umbraco.Core;
using Umbraco.Web.WebApi;


public class ReportApiController : UmbracoApiController
{
    /// <summary>
    /// Gets logged members survey lists
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public HttpResponseMessage GetReportData(int Id)
    {
        var ReportData = ReportHelper.GetReportData(Id);
        //var ReportRepo = new ReportRepo();
        //var compResults = ReportRepo.GetCompetencyResults(Id, "2019/1");

        //var ReportData = new Reports();
        //ReportData.CompetencyResults = ReportHelper.FormatCompetencyResults(compResults);
        //ReportData.SpiderResults = ReportRepo.GetSpiderData(Id, "2019/1");

        return Request.CreateResponse(HttpStatusCode.OK, ReportData, new JsonMediaTypeFormatter());
    }

   

}
