﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Http;
using umbraco;
using Umbraco.Core;
using Umbraco.Web.WebApi;


public class SurveyApiController : UmbracoApiController
{

    [HttpPost]
    public HttpResponseMessage GenerateAll(List<UserDetails> UserList, string TemplateId = "2A7E3554-72D0-4528-A7D6-787F74A8605B")
    {
        var updatedAssigned = new List<AssignedSurveys>();
        var appRepo = new AppraisalRepo();
        // var batchId = new Guid(TemplateId);
        var TemplateQuestions = appRepo.GetTemplateQuestions(TemplateId);
        foreach (var ul in UserList)
        {
            
            AppraisalHelper.CreateSurveys(ul.AssignedList, TemplateQuestions);
        }

        var repo = new AssignedSurveyRepo();

        return Request.CreateResponse(HttpStatusCode.OK, updatedAssigned, new JsonMediaTypeFormatter());
    }

    /// <summary>
    /// Gets logged members survey lists
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public HttpResponseMessage GetUserSurveyList()
    {
        var SurveyTypes = new SurveyLists();
        var User = EmployeeHelper.GetUser();
        var awaitingTotals= UserAssignHelper.GetTotals(User.Id, "2019/1");

        SurveyTypes.CompletedBy = UserAssignHelper.GetAssigned(User.Id, "2019/1");
        SurveyTypes.SelfAssessment = SurveyTypes.CompletedBy.Exists(x => x.ForMember == User.Id) ? SurveyTypes.CompletedBy.Find(x => x.ForMember == User.Id) : new AssignedSurveys();
        SurveyTypes.AwaitingTotal = awaitingTotals.FinalScore;
        SurveyTypes.AwaitingYourInput = awaitingTotals.InCompleted;
        return Request.CreateResponse(HttpStatusCode.OK, SurveyTypes, new JsonMediaTypeFormatter());

    }

    /// <summary>
    /// Get questions for unique survey form page
    /// </summary>
    /// <param name="Guid"></param>
    /// <returns></returns>
    [HttpGet]
    public HttpResponseMessage GetSurveyQuestions(string Guid)
    {
        var surveyRepo = new SurveyRepository();
        var surveyQuestions = surveyRepo.GetSurveyQuestions(Guid);
        var newQuestionList = new List<AppraisalAnswers>();
        var qNum = 1;
        foreach (var question in surveyQuestions)
        {
            if(question.IsValue)
            {
                newQuestionList.Add(question);
            }
            if (question.IsCategory)
            {
                var tempList = new List<AppraisalAnswers>();
                question.QuestionList = surveyQuestions.FindAll(x=>x.CompetencyId==question.CompetencyId && x.IsCategory==false);
                foreach(var q in question.QuestionList)
                {
                    q.Answer = 3;
                    q.QuestionNumber = qNum;
                    qNum++;
                }
                newQuestionList.Add(question);
            }
            
        }

        return Request.CreateResponse(HttpStatusCode.OK, newQuestionList, new JsonMediaTypeFormatter());
    }

    /// <summary>
    /// Get survey info for management portal page
    /// </summary>
    /// <param name="MemberList"></param>
    /// <returns></returns>
    [HttpGet]
    public  HttpResponseMessage GetSurveyInfoByMember()
    {
        var surveyRepo = new SurveyRepository();
        var member = EmployeeHelper.GetUser();
        var EmployeeList = EmployeeHelper.GetEmployeesID(member);
        var surveyInfo = surveyRepo.GetSurveyDetails(EmployeeList);

        return Request.CreateResponse(HttpStatusCode.OK, surveyInfo, new JsonMediaTypeFormatter());
    }

    /// <summary>
    /// submit survey answers back to db and change status
    /// </summary>
    /// <param name="SurveyQuestions"></param>
    /// <returns></returns>
    [HttpPost]
    public HttpResponseMessage SubmitSurvey(List<AppraisalAnswers> SurveyQuestions)
    {
        var assignRepo = new AssignedSurveyRepo();
        var surveyRepo = new SurveyRepository();

        var surveyGuid = SurveyQuestions[0].Guid.ToString(); //grab Guid string

        var userSurvey = surveyRepo.GetUserSurvey(surveyGuid); //to get survey batch

        foreach (var us in userSurvey) //update status +completed date
        {
            us.status = 3;
            us.CompletedDate = DateTime.UtcNow;

            assignRepo.AddAssigned(us);
        }

        AppraisalHelper.AddQuestions(SurveyQuestions);

        return Request.CreateResponse(HttpStatusCode.OK, userSurvey, new JsonMediaTypeFormatter());

    }


    [HttpGet]
    public HttpResponseMessage GetSelfSurveyInfo()
    {
        var surveyRepo = new SurveyRepository();
        var member = EmployeeHelper.GetUser();
        var ResultsList = surveyRepo.GetSelfSurveyStatus(member.Id, "2019/1");//EmployeeHelper.GetEmployeesID(member);
      //  var surveyInfo = surveyRepo.GetSurveyDetails(EmployeeList);

        return Request.CreateResponse(HttpStatusCode.OK, ResultsList, new JsonMediaTypeFormatter());
    }

}
