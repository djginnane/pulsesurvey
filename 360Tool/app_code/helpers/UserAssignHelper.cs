﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Web;
using Umbraco.Core;
using Umbraco.Core.Models;
using System.Web.Security;

/// <summary>
/// Creates a list to be choose who to assign to each individuals surveys
/// </summary>
public class UserAssignHelper
{
    public static List<UserDetails> GetAssigned(string Description = "2019/1")
    {
        var repo = new AssignedSurveyRepo();
        var Members = EmployeeHelper.GetAllMembers();
        var AssignedList = new List<UserDetails>();
        var assignedList = repo.GetUsersAssigned(Description);

        foreach (var m in Members)
        {
            var au = new UserDetails();
            var man = EmployeeHelper.GetManager(m);
            au.MemberName = m.Name;
            au.MemberId = m.Id;

            au.ManagerList = assignedList.Exists(x => x.ForMember == m.Id && x.SurveyType == "Manager") ? assignedList.FindAll(x => x.ForMember == m.Id && x.SurveyType == "Manager") : new List<AssignedSurveys>();
            au.AssignedList = assignedList.Exists(x => x.ForMember == m.Id) ? assignedList.FindAll(x => x.ForMember == m.Id) : new List<AssignedSurveys>();
            if (man != null)
            {
                au.ManagerId = man.Id;
                au.ManagerName = man.Name;

                /// if there are no managers listed add the users manager to the list
                if (au.ManagerList.Count == 0)
                {
                    var manAdd = new AssignedSurveys();
                    manAdd.status = 0;
                    manAdd.SurveyType = "Manager";
                    manAdd.ForMember = m.Id;
                    manAdd.CompletedBy = man.Id;
                    manAdd.CompletedByName = man.Name;
                    manAdd.Description = Description;
                    manAdd.MemberName = m.Name;
                    au.ManagerList.Add(manAdd);
                    au.AssignedList.Add(manAdd);
                }
            }

            au.SubBoardinateList = new List<AssignedSurveys>();
            au.SubBoardinateList = assignedList.Exists(x => x.ForMember == m.Id && x.SurveyType == "subordinate") ? assignedList.FindAll(x => x.ForMember == m.Id && x.SurveyType == "subordinate") : new List<AssignedSurveys>();
            au.PierList = assignedList.Exists(x => x.ForMember == m.Id && x.SurveyType == "pier") ? assignedList.FindAll(x => x.ForMember == m.Id && x.SurveyType == "pier") : new List<AssignedSurveys>();



            AssignedList.Add(au);
        }

        return AssignedList;

    }
    public static AssignedSurveys MapNewAssigned (AssignedSurveys AL, string TemplateId)
    {
        AL.status = 1;
        AL.DateAssigned = DateTime.UtcNow;
        AL.TemplateId = new Guid(TemplateId);
        AL.SurveyBatch = Guid.NewGuid();
        return AL;
    }
    public static List<AssignedSurveys> GetAssigned(int memberId, string Description = "2019/1")
    {
        var repo = new SurveyRepository();
        //var repo = new AssignedSurveyRepo();
        var surveyList = repo.GetUserAssigned(memberId, Description);
        return surveyList;

    }

    public static AssignedSurveys GetTotals(int memberId, string Description = "2019/1")
    {
        var repo = new SurveyRepository();
        //var repo = new AssignedSurveyRepo();
        var surveyList = repo.GetSurveyStatus(memberId, Description);
        return surveyList;
    }
}
