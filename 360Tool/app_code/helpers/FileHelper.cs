﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Umbraco.Web;
using Umbraco.Core;
using System.Diagnostics;
using Umbraco.Core.Models;

/// <summary>
/// Summary description for FileHelper
/// </summary>
public class FileHelper
{
    public static List<UserTemp> Main(string FileName)
    {
        var LineTxt = "";
        List<string> listA = new List<string>();
        var CustomerList = new List<UserTemp>();
        string path = @FileName;
        try
        {
            //if (File.Exists(path))
            //{
            //    File.Delete(path);
            //}

            //using (StreamWriter sw = new StreamWriter(path))
            //{
            //    sw.WriteLine("This");
            //    sw.WriteLine("is some text");
            //    sw.WriteLine("to test");
            //    sw.WriteLine("Reading");
            //}
            var i = 0;


            using (StreamReader sr = new StreamReader(path))
            {
                while (sr.Peek() >= 0)
                {
                    i++;
                    LineTxt = sr.ReadLine();
                    // Console.WriteLine(sr.ReadLine());
                    if (i != 1)
                    {

                        int delimeter = LineTxt.LastIndexOf('\t');
                        LineTxt = Regex.Replace(LineTxt, @"\B""\b[^""]+\b""\B", m => m.Value.Replace(",", ""));
                        var values = new string[30];


                        if (delimeter != -1)
                        {
                            values = LineTxt.Split('\t');
                        }
                        else
                        {
                            LineTxt = LineTxt.Replace("\"", "");
                            values = LineTxt.Split(',');
                        }
                        var sizeVal = values.Length;

                        UserTemp importList = new UserTemp();
                        //string s = values[0];
                        //int idx = s.LastIndexOf(' ');

                        //int phone = p.LastIndexOf("p:");

                        //var tempPhone = (p.Substring(phone + 1));
                        decimal func;
                        bool funcDec = decimal.TryParse(values[5], out func);
                        decimal Comp;
                        bool compDec = decimal.TryParse(values[6], out Comp);


                        importList.Name = values[0];

                        importList.Level = values[1];
                        importList.Organisation = values[2];
                        importList.Country = values[3];
                       
                        importList.Email = sizeVal > 4 ? values[4] : "";
                        importList.ManagerEmail = sizeVal > 5 ? values[5] : "";
                        var IsManager = sizeVal > 6 ? values[6] : "";
                        importList.IsManager = IsManager.ToLower() == "yes" ? true : false;
                        var IsExcluded = sizeVal > 7 ? values[7] : "";
                        importList.IsExcluded = IsExcluded.ToLower() == "yes" ? true : false;
                        CustomerList.Add(importList);
                    }

                }
            }

        }
        catch (Exception e)
        {
            Console.WriteLine("The process failed: {0}", e.ToString());
            CustomerList = new List<UserTemp>();

        }

        // AddCustomers(CustomerList);
        return CustomerList;
    }
    public static void AddUsers(List<UserTemp> UserList)
    {
        //TODO: Add root hubs after login is done, delete users based on type and root hub data is being uploaded
        var noManagersList = new List<IMember>();
        var LoggedMember = EmployeeHelper.LoggedInMember;
        var hub = LoggedMember.GetValue<Udi>("hub");
        var MemberService = ApplicationContext.Current.Services.MemberService;
        var getMemberList = EmployeeHelper.GetAllMembers();
    //    var PageSettings = ContentHelpers.getSettings();
        if (UserList.Count != 0)
        {
          //  DeleteUsers(getMemberList.ToList());

        }
        var memType = ApplicationContext.Current.Services.MemberTypeService.Get("Staff");
        foreach (var em in UserList)
        {
            string tempEmail = String.IsNullOrWhiteSpace(em.Email) ? Path.GetRandomFileName() + "@someemail.com" : em.Email;
            var Manager = ApplicationContext.Current.Services.MemberService.GetByEmail(em.ManagerEmail);

            if (!getMemberList.Exists(x=>x.Email==em.Email))
            {

                var NewMember = MemberService.CreateMemberWithIdentity(tempEmail, tempEmail, em.Name, "Member");
                //   var NewMember = new Member(em.Name, tempEmail, tempEmail, "888888888888", memType);
                if (Manager != null)
                {
                    NewMember.SetValue("manager", Manager.GetUdi().ToString());
                }
                NewMember.IsApproved = true;
                //    NewMember.SetValue("baseSalary", em.BaseSalary);
                NewMember.SetValue("organisation", 154);
                NewMember.SetValue("country", 155);
                //    NewMember.SetValue("functionalScore", em.Functional);
                //   NewMember.SetValue("competencyScore", em.Competency);
                //   NewMember.SetValue("businessUnit", em.Business_Unit);
                NewMember.SetValue("hub", hub.ToString());
                //  MemberService.Save(NewMember);


                NewMember.SetValue("managersEmail", em.ManagerEmail);
                NewMember.SetValue("isManager", em.IsManager);
                NewMember.SetValue("excludeFromResults", em.IsExcluded);




                //  MemberService.AssignRole(NewMember.Id, "Mobile Users");
                MemberService.Save(NewMember);
                MemberService.SavePassword(NewMember, "8888888888");
                MemberService.AssignRole(NewMember.Id, "Baware");
                //  MemberService.AssignRole(NewMember.Id, "Kpi");
                if (Manager == null)
                {
                    noManagersList.Add(NewMember);
                }
            }
        }
        setLostManagers(noManagersList);
    }
    public static void AddUsersTemp(List<UserTemp> UserList)
    {
        var db = ApplicationContext.Current.DatabaseContext.Database;
        foreach (var user in UserList)
        {
            db.Save(user);
        }
    }
    public static void setLostManagers(List<IMember> memberList)
    {
        var MemberService = ApplicationContext.Current.Services.MemberService;

        foreach (var ml in memberList)
        {
            var Manager = MemberService.GetByEmail(ml.GetValue<string>("managersEmail"));
            if (Manager != null)
            {
                ml.SetValue("manager", Manager.GetUdi().ToString());
                MemberService.Save(ml);
            }

        }
    }
    public static void DeleteUsers(List<IMember> MemberList)
    {
        var MemberService = ApplicationContext.Current.Services.MemberService;

        foreach (var st in MemberList)
        {
            //  Debug.WriteLine("Send to debug output." + st.Id);
            MemberService.Delete(st);
        }

    }
    public FileHelper()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}