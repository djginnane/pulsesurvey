﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


    public class AppraisalHelper
    {

    public static List<AppraisalAnswers> RetrieveAppraisal(string guid, string selfAssess)
    {
        var repo = new AppraisalRepo();
        var employeeId = 1151;
        var UserRecord = CheckAppraisals(guid,  employeeId);
        var AppraisalList = new List<AppraisalAnswers>();
        if (UserRecord.QuestionsStatus == 1)
        {
            CreateAppraisalQuestions(UserRecord);
            AppraisalList = repo.GetUserAppraisal(employeeId, guid);
            ///Create questions
        }
        else if (UserRecord.QuestionsStatus == 2)
        {
            /// grab existing
            AppraisalList = repo.GetUserAppraisal(employeeId, guid);
        }
        
        
        return new List<AppraisalAnswers>();
    }

    public static AppraisalSetup CheckAppraisals(string guid, int employeeId)
    {
        var repo = new AppraisalRepo();
        var UserRecord = repo.CheckUserAppraisal(employeeId, guid);
       
        return UserRecord;

    }
    public static List<AppraisalAnswers> CreateAppraisalQuestions(AppraisalSetup UserAppraisal)
    {

        /// get all questions from template
        var repo = new AppraisalRepo();
        var genericRepo = new GenericRepo<AppraisalAnswers>();
        var genericRepo2 = new GenericRepo<AppraisalSetup>();
        var questions = repo.GetTemplateQuestions(UserAppraisal.templateId.ToString());
        foreach(var q in questions)

        {
            var newq = new AppraisalAnswers();
            newq.Guid = UserAppraisal.AppraisalBatch;
            newq.QuestionTmeplateId = q.Id;
            newq.MemberId = UserAppraisal.MemberId;
            newq.questionType = q.FormType;
            newq.UserId = UserAppraisal.UserId;
            newq.FormType = q.FormType;
            newq.sortOrder = q.sortOrder;
            newq.IsCategory = q.IsCategory;

            genericRepo.InsertAppraisal(newq);
        }

        UserAppraisal.QuestionsStatus = 2;
        genericRepo2.SaveAppraisal(UserAppraisal);
        return new List<AppraisalAnswers>();
    }

    public static List<AssignedSurveys> CreateSurveys(List<AssignedSurveys> Surveys, List<AppraisalTemplate> templateQs, string TemplateId = "2A7E3554-72D0-4528-A7D6-787F74A8605B")
    {
        var assignRepo = new AssignedSurveyRepo();

        foreach(var s in Surveys)
        {
            ///first check no appraisal has been generated first
            if (s.status <= 1)
            {
                var QuestionSet = new List<AppraisalAnswers>();
                var i = 0;
                foreach (var tq in templateQs)
                {
                    i++;
                    var question = new AppraisalAnswers();
                    question.sortOrder = i;
                    question.MemberId = s.CompletedBy;
                    question.UserId = s.ForMember;
                    question.QuestionTmeplateId = tq.Id;
                    question.IsCategory = tq.IsCategory;
                    question.IsValue = tq.IsValue;
                    question.Guid = s.SurveyBatch;
                    question.CompetencyId = tq.CompetencyId;
                    question.ValueId = tq.ValueId;
                    QuestionSet.Add(question);
                }
                AddQuestions(QuestionSet);
                var newAssign = s;
                if (s.Id == 0)
                   
                {
                    newAssign = UserAssignHelper.MapNewAssigned(newAssign, TemplateId);
                }
                newAssign.status = 2;
                newAssign.DateGenerated = DateTime.UtcNow;
               
                ///Update the survey status
                assignRepo.AddAssigned(s);
            }
        }

        return Surveys;
    }

    public static void AddQuestions(List<AppraisalAnswers> answers)
    {
        var genRepo = new GenericRepo<AppraisalAnswers>();
        foreach (var a in answers)
        {
            if (a.IsCategory)
            {
                foreach(var an in a.QuestionList)
                {

                    genRepo.SaveAppraisal(an);
                }
                
            }
         
        }
    }
    public static void AddQuestions2(List<AppraisalAnswers> answers)
    {
        var genRepo = new GenericRepo<AppraisalAnswers>();
        foreach (var a in answers)
        {
            if (a.IsCategory)
            {
                foreach(var an in a.QuestionList)
                {
                    genRepo.SaveAppraisal(a);
                }
            }
           
        }
    }
}
