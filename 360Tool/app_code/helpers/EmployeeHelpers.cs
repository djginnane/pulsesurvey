﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Web;
using Umbraco.Core;
using Umbraco.Core.Models;
using System.Web.Security;

/// <summary>
/// Summary description for MemberService
/// </summary>
public class EmployeeHelper
{

    public static IMember GetManager(IMember mem)
    {
        var umbracoHelper = new Umbraco.Web.UmbracoHelper(Umbraco.Web.UmbracoContext.Current);
        var manager = mem.GetValue<Udi>("manager");
        var id = umbracoHelper.GetIdForUdi(manager);
        var man = EmployeeHelper.GetById(id);
        return man;
    }

    public static List<int> GetEmployeesID(IMember mem)
    {
        var umbracoHelper = new Umbraco.Web.UmbracoHelper(Umbraco.Web.UmbracoContext.Current);
        var allMem = GetAllMembers().Where(x => x.GetValue<Udi>("manager").ToString() == mem.GetUdi().ToString());
        var idList = new List<int>();
      foreach(var m in allMem)
        {
            idList.Add(m.Id);
        }
        return idList;
    }

    public static List<IMember> GetAllMembers()
    {
        var umbracoHelper = new Umbraco.Web.UmbracoHelper(Umbraco.Web.UmbracoContext.Current);
        var Members = ApplicationContext.Current.Services.MemberService.GetAllMembers().ToList();

        return Members;

    }
    public static List<IMember> GetMembersByOrgan(string organisation, string country)
    {
        var umbracoHelper = new Umbraco.Web.UmbracoHelper(Umbraco.Web.UmbracoContext.Current);
        var Members = ApplicationContext.Current.Services.MemberService.GetAllMembers().Where(x=>x.GetValue<string>("organisation")== organisation).ToList();

        return Members;

    }

    public static List<string> GetEmployeesOrg(IMember mem)
    {
        var umbracoHelper = new Umbraco.Web.UmbracoHelper(Umbraco.Web.UmbracoContext.Current);
        var allMem = GetAllMembers().Where(x => x.GetValue<Udi>("manager").ToString() == mem.GetUdi().ToString());
        var orgList = new List<string>();
        foreach (var m in allMem)
        {
            orgList.Add(m.GetValue<string>("organisation"));
        }
        return orgList;
    }

    public static IMember GetMemberByUdi(Udi ud)
    {
        var umbracoHelper = new Umbraco.Web.UmbracoHelper(Umbraco.Web.UmbracoContext.Current);
        // var manager = mem.GetValue<Udi>("manager");
        var id = umbracoHelper.GetIdForUdi(ud);
        var man = EmployeeHelper.GetById(id);
        return man;
    }
    public static IMember GetById(int ID)
    {
        return ApplicationContext.Current.Services.MemberService.GetById(ID);
    }
    public static IMember GetByUsername(string Username)
    {

        return ApplicationContext.Current.Services.MemberService.GetByUsername(Username);
    }
    public static IMember GetUser()
    {
        ///test data, replace with below once security is added
        //return ApplicationContext.Current.Services.MemberService.GetById(1171);

        return LoggedInMember;
    }


    public static string CurrentLoggedInMemberName
    {
        get
        {
            var currentSession = HttpContext.Current.Session;
            if (currentSession == null)
            {
                return @Membership.GetUser().UserName;
            }
            else if (currentSession["LoggedInMemberName"] == null)
            {
                currentSession["LoggedInMemberName"] = @Membership.GetUser().UserName;
            }
            return (string)currentSession["LoggedInMemberName"];
        }
    }
    public static IMember LoggedInMember
    {
        get
        {
            var currentSession = HttpContext.Current.Session;
            if (currentSession == null)
            {
                return ApplicationContext.Current.Services.MemberService.GetByUsername(CurrentLoggedInMemberName);
            }
            else if (currentSession["MemberProfile"] == null)
            {
                currentSession["MemberProfile"] = ApplicationContext.Current.Services.MemberService.GetByUsername(CurrentLoggedInMemberName);

            }

            return (IMember)currentSession["MemberProfile"];
        }
    }
    public class EmployeeHelpers
    {
    }
}