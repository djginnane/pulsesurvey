﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;
using System.Web.Mvc;


public class DataHelpers
    {
    public static int CurrentHub(IMember user)
    {
        var umbracoHelper = new Umbraco.Web.UmbracoHelper(Umbraco.Web.UmbracoContext.Current);
        var hub = user.GetValue<Udi>("hub");
        var content = umbracoHelper.TypedContent(hub);


        return content.Id;
    }

  
}
