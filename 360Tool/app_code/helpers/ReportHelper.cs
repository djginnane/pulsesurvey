﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Web;
using Umbraco.Core;
using Umbraco.Core.Models;
using System.Web.Security;



public class ReportHelper
    {
    public static Reports GetReportData(int Id)
    {
        var ReportRepo = new ReportRepo();
        var compResults = ReportRepo.GetCompetencyResults(Id, "2019/1");
        var compSummary = ReportRepo.GetCompetencySummary(Id, "2019/1");

        var ReportData = new Reports();
        ReportData.CompetencyResults = ReportHelper.FormatCompetencyResults(compResults);
        ReportData.SpiderResults = ReportRepo.GetSpiderData(Id, "2019/1");
        ReportData.CompetencySummary = compSummary;
        return ReportData;
    }

    /// <summary>
    /// Put results in the format of competency score broken name by group type
    /// </summary>
    public static List<resultsTable> FormatCompetencyResults(List<resultsTable> compInfo)
    {
        var tempReport = new List<resultsTable>();
      var  tempCom = "None Set";
        var comp = new List<resultsTable>();
      var   Compentecy = new resultsTable();
        foreach (var rs in compInfo)
        {
          
           
           
            if (rs.CompetencyName!=tempCom)
            {
                if (tempCom != "None Set")
                {
                    Compentecy.CompetencyList = comp;
                    tempReport.Add(Compentecy);

                }
                comp= new List<resultsTable>();
                Compentecy = new resultsTable();
                Compentecy.CompetencyName = rs.CompetencyName;
                Compentecy.CompetencyId = rs.CompetencyId;
                Compentecy.CompetencyDescription = rs.CompetencyDescription;
                var userType = new resultsTable();
                userType = rs;
                comp.Add(userType);
            }
            else
            {
                var userType = new resultsTable();
                userType= rs;
                comp.Add(userType);
            }
           tempCom = rs.CompetencyName;
        }
        Compentecy.CompetencyList = comp;
        tempReport.Add(Compentecy);
        return tempReport;
    }
    }
