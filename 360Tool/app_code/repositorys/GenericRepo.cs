﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Persistence;


    public class GenericRepo<t>
    {
    private readonly UmbracoDatabase db;

    public t DeleteAppraisal(t Appraisal)
    {

        db.Delete(Appraisal);
        return Appraisal;

    }
    public t SaveAppraisal(t Appraisal)
    {

        db.Save(Appraisal);
        return Appraisal;

    }
    public t InsertAppraisal(t Appraisal)
    {

        db.Insert(Appraisal);
        return Appraisal;

    }

    public GenericRepo()
    {
        db = ApplicationContext.Current.DatabaseContext.Database;
        //
        // TODO: Add constructor logic here
        //
    }

}
