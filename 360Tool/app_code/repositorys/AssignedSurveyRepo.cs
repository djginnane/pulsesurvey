﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Persistence;


    public class AssignedSurveyRepo
    {
        private readonly UmbracoDatabase db;


        public List<AssignedSurveys> GetUsersAssigned(string Description)
        {
            var lt = new List<AssignedSurveys>();
            lt = db.Fetch<AssignedSurveys> ("SELECT * FROM [360Tool].[dbo].[360AssignedSurvey] where Description = @0 order by ForMember, SurveyType desc", Description);

            lt = lt == null ? new List<AssignedSurveys>() : lt;
            return lt;


        }

    public AssignedSurveys AddAssigned(AssignedSurveys AssSur)
    {
        db.Save(AssSur);
        return AssSur;
    }
        public AssignedSurveyRepo()
    {
        db = ApplicationContext.Current.DatabaseContext.Database;
        //
        // TODO: Add constructor logic here
        //
    }
}
