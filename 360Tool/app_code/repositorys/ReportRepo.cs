﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Persistence;


public class ReportRepo
    {
    private readonly UmbracoDatabase db;

    /// <summary>
    /// data for comparing value scores submitted by different groups.
    /// </summary>
    /// <param name="MemberId"></param>
    /// <param name="Description"></param>
    /// <returns></returns>
    public List<SpiderChart> GetSpiderData(int MemberId, string Description)
    {
        var lt = new List<SpiderChart>();
        lt = db.Fetch<SpiderChart>("select avg(answer) as AverageScore, [guid],  ValueId, [360AssignedSurvey].SurveyType as UserType, ValueName   from [360AppraisalAnswers] left join [360AssignedSurvey] on guid=surveyBatch join [360SurveyValues] on [360SurveyValues].Id=ValueId where UserId=@0 and [360AssignedSurvey].Description =@1 and [status]=3 and isValue!='true' and IsCategory!='True' group by [guid],  [ValueId], SurveyType, ValueName ", MemberId, Description);

        lt = lt == null ? new List<SpiderChart>() : lt;
        return lt;


    }

    /// <summary>
    /// Tables of competency scores broken down by managers/self etc..
    /// </summary>
    /// <param name="MemberId"></param>
    /// <param name="Description"></param>
    /// <returns></returns>
    public List<resultsTable> GetCompetencyResults(int MemberId, string Description)
    {
        var compResults = db.Fetch<resultsTable>("select avg(Answer) as Answer, CompetencyId,  CompetencyName, SurveyType, GenericDescription as CompetencyDescription from [dbo].[360AppraisalAnswers] join [360SurveyCompetencies] on [360SurveyCompetencies].Id=[360AppraisalAnswers].CompetencyId join [360AssignedSurvey] on [360AssignedSurvey].SurveyBatch=[360AppraisalAnswers].guid where UserId=@0 and Isvalue!='true' and IsCategory!='true' and [360AssignedSurvey].[status]=3 group by  CompetencyId, CompetencyName, SurveyType,GenericDescription order by CompetencyId", MemberId);
        compResults = compResults == null ? new List<resultsTable>() : compResults;
        return compResults;
    }
    /// <summary>
    /// Competency Report Summary
    /// </summary>
    /// <param name="MemberId"></param>
    /// <param name="Description"></param>
    /// <returns></returns>
    public List<competencyTable> GetCompetencySummary(int MemberId, string Description)
    {
        var compResults = db.Fetch<competencyTable>("select avg(answer) as TotalAverage, CompetencyName, ValueName, avg(case when SurveyType='manager' then answer end) as managerAverage, avg(case when SurveyType='subordinate' then answer end) as subordinateAverage, avg(case when SurveyType='pier' then answer end) as pierAverage, avg(case when SurveyType='self' then answer end) as selfAssessment  FROM [dbo].[360AppraisalAnswers] join [360SurveyValues] on [360SurveyValues].[id]=[ValueId] join [360SurveyCompetencies] on [360SurveyCompetencies].[id]=[CompetencyId] join [360AssignedSurvey] on [360AssignedSurvey].SurveyBatch=[360AppraisalAnswers].guid where UserId=@0 and Isvalue!='true' and IsCategory!='true' and [status]=3 group by CompetencyId, CompetencyName, ValueName", MemberId);
        compResults = compResults == null ? new List<competencyTable>() : compResults;
        return compResults;
    }

    public ReportRepo()
    {
        db = ApplicationContext.Current.DatabaseContext.Database;
        //
        // TODO: Add constructor logic here
        //
    }
}


