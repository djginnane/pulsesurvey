﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Persistence;

public class AppraisalRepo
    {
    private readonly UmbracoDatabase db;


    public List<AppraisalAnswers> GetUserAppraisal(int MemberId, string Guidapp)
    {
        var lt = new List<AppraisalAnswers>();
        lt = db.Fetch<AppraisalAnswers>("select * from from [360AppraisalAnswers] where MemberId=@0 and Guid=@1 ",  MemberId, Guidapp);

        lt = lt == null ? new List<AppraisalAnswers>() : lt;
        return lt;


    }

    public AppraisalSetup CheckUserAppraisal(int MemberId, string Guidapp)
    {
        var lt = new List<AppraisalSetup>();
       lt = db.Fetch<AppraisalSetup> ("select * from [360AppraisalSetup] where MemberId=@0 and AppraisalBatch=@1 ", MemberId, Guidapp);
        var details = new AppraisalSetup();
        lt = lt == null ? new List<AppraisalSetup>() : lt;
        foreach (var l in lt)
        {
            details = l;
        }
        return details;


    }

    public List<AppraisalTemplate> GetTemplateQuestions( string Guidtemp)
    {
        var lt = new List<AppraisalTemplate>();
        lt = db.Fetch<AppraisalTemplate>("select * from [360AppraisalTemplate] where TemplateId=@0 order by sortOrder ", Guidtemp);

        lt = lt == null ? new List<AppraisalTemplate>() : lt;
        return lt;


    }
    public AppraisalRepo()
    {
        db = ApplicationContext.Current.DatabaseContext.Database;
        //
        // TODO: Add constructor logic here
        //
    }

}
