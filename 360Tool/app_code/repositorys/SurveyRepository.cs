﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Persistence;

    public class SurveyRepository
    {

        private readonly UmbracoDatabase db;
        public List<AppraisalAnswers> GetUserAppraisal(int MemberId, string Guidapp)
        {
            var lt = new List<AppraisalAnswers>();
            lt = db.Fetch<AppraisalAnswers>("select * from  [360AppraisalAnswers] where MemberId=@0 and Guid=@1 ", MemberId, Guidapp);

            lt = lt == null ? new List<AppraisalAnswers>() : lt;
            return lt;


        }

        /// <summary>
        /// Gets surveys needed to be completed by a user
        /// </summary>
        /// <param name="MemberId"></param>
        /// <returns></returns>
        public List<AssignedSurveys> GetUserAssigned(int MemberId, string surveyDesc)
        {
            var lt = new List<AssignedSurveys>();
            lt = db.Fetch<AssignedSurveys>("select * from [360AssignedSurvey] where CompletedBy=@0 and status>=2 and Description=@1", MemberId, surveyDesc);

            lt = lt == null ? new List<AssignedSurveys>() : lt;
            return lt;


        }

        /// <summary>
        /// Gets the amount of surveys completed / incomplete that a user needs to finish
        /// </summary>
        /// <param name="MemberId"></param>
        /// <returns></returns>
        public AssignedSurveys GetSurveyStatus(int MemberId, string surveyDesc)
        {
            var lt = new List<AssignedSurveys>();
            lt = db.Fetch<AssignedSurveys>("select CompletedBy, Count(Id) as FinalScore, sum(case when [360AssignedSurvey].[status]=2 then 1 end ) as InCompleted,  sum(case when [360AssignedSurvey].[status]=3 then 1 end ) as Completed   from [360AssignedSurvey] where CompletedBy=@0 and status>=2 and Description=@1 group by CompletedBy", MemberId, surveyDesc);
            var totals = new AssignedSurveys();
           foreach (var s in lt)
            {
                totals = s;
            }
            return totals;


        }


    /// <summary>
    /// Gets the amount of surveys completed / incomplete of a particular user
    /// </summary>
    /// <param name="MemberId"></param>
    /// <returns></returns>
    public AssignedSurveys GetSelfSurveyStatus(int MemberId, string surveyDesc)
    {
        var lt = new List<AssignedSurveys>();
        lt = db.Fetch<AssignedSurveys>("select sum(case status when 2 then 1 else 0 end) as Incompleted, sum(case status when 3 then 1 else 0 end) as FinalScore from [360AssignedSurvey] where ForMember=@0 and Description=@1 and status>=2", MemberId, surveyDesc);
        var totals = new AssignedSurveys();
        foreach (var s in lt)
        {
            totals = s;
        }
        return totals;


    }
    /// <summary>
    /// group completed/total surveys by Member ID 
    /// </summary>
    /// <param name="MemberList"></param>
    /// <returns></returns>
    public List<SurveyCompleted> GetSurveyDetails(List<int> MemberList)
    {
        var lt = new List<SurveyCompleted>();
        lt = db.Fetch<SurveyCompleted>("select ForMember, MemberName, count(ForMember) as total, count(case status when 3 then 1 else null end) as completed from [360AssignedSurvey] where forMember in (@0) group by ForMember, MemberName", MemberList.ToArray());
        lt = lt == null ? new List<SurveyCompleted>() : lt;
        return lt;
    }

        
        /// <summary>
        /// Gets all survey questions to build survey
        /// </summary>
        public List<AppraisalAnswers> GetSurveyQuestions(string Guid)
        {
            var lt = new List<AppraisalAnswers>();
            lt = db.Fetch<AppraisalAnswers>("select [360AppraisalAnswers].*, [360AppraisalTemplate].question as Question, [360SurveyValues].ValueName ,  [360SurveyCompetencies].CompetencyName from [360AppraisalAnswers] inner join [360AppraisalTemplate] on [360AppraisalAnswers].QuestionTmeplateId = [360AppraisalTemplate].Id left join [360SurveyValues] on [360AppraisalTemplate].ValueId = [360SurveyValues].Id left join [360SurveyCompetencies] on [360AppraisalAnswers].CompetencyId = [360SurveyCompetencies].Id where Guid = @0 order by [360AppraisalTemplate].sortOrder", Guid);
            lt = lt == null ? new List<AppraisalAnswers>() : lt;
            return lt;
        }
        
        /// <summary>
        /// Grab assigned user survey by Guid 
        /// </summary>
        /// <param name="Guid"></param>
        /// <returns></returns>
        public List<AssignedSurveys> GetUserSurvey(string Guid)
        {
            var lt = new List<AssignedSurveys>();
            lt = db.Fetch<AssignedSurveys>("select * from [360AssignedSurvey] where status>=2 and SurveyBatch=@0", Guid);

            lt = lt == null ? new List<AssignedSurveys>() : lt;
            return lt;


        }
    public SurveyRepository()
            {
                db = ApplicationContext.Current.DatabaseContext.Database;
                //
                // TODO: Add constructor logic here
                //
            }

  

  
    }