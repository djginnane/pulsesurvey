﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Persistence;

/// <summary>
/// Create custom tables
/// </summary>

public class RegisterEvents : ApplicationEventHandler
{
    protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
    {
        var ctx = applicationContext.DatabaseContext;
        var db = new DatabaseSchemaHelper(ctx.Database, applicationContext.ProfilingLogger.Logger, ctx.SqlSyntax);
        if (!db.TableExist("360Appraisal"))
        {
            db.CreateTable<Appraisal>(false);
        }

        if (!db.TableExist("360AppraisalSetup"))
        {
            db.CreateTable<AppraisalSetup>(false);
        }

        if (!db.TableExist("360AppraisalAnswers"))
        {
            db.CreateTable<AppraisalAnswers>(false);
        }

        if (!db.TableExist("360AppraisalTemplate"))
        {
            db.CreateTable<AppraisalTemplate>(false);
        }
        if (!db.TableExist("360SurveyValues"))
        {
            db.CreateTable<SurveyValues>(false);
        }
        if (!db.TableExist("360SurveyCompetencies"))
        {
            db.CreateTable<SurveyCompetencies>(false);
        }
        if (!db.TableExist("360AssignedSurvey"))
        {
            db.CreateTable<AssignedSurveys>(false);
        }
        if (!db.TableExist("360UploadCsv"))
        {
            db.CreateTable<UploadedCsv>(false);
        }
        if (!db.TableExist("360CompetencyAnalysis"))
        {
            db.CreateTable<CompetencyAnalysis> (false);
        }
        if (!db.TableExist("360ValueAnalysis"))
        {
            db.CreateTable<ValueAnalysis>(false);
        }
    }

}