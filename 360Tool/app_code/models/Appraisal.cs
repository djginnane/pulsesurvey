﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

[TableName("360AppraisalForms")]
[PrimaryKey("Id", autoIncrement = true)]
public class Appraisal
{
    // The id of the person who the appraisal is about
    public string AppraisalName { get; set; }
    public int OwnersMemberId { get; set; }
    public Guid TemplateId { get; set; }
    [PrimaryKeyColumn(AutoIncrement = true)]
    public int Id { get; set; }

    // the id of the employee completing the apraisal for another employee
    public int EmployeeMemberId { get; set; }
 
    /// <summary>
    /// Unquique id assigned to all appraisals for one particalar user for the time frame
    /// </summary>
    public Guid AppraisalBatch { get; set; }
    [ResultColumn]
    public string Question { get; set; }
    [ResultColumn]
    [NullSetting(NullSetting = NullSettings.Null)]
    public string Category { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public int ValueId { get; set; }
    public int CompetencyId { get; set; }
    public DateTime DateCreated { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public DateTime DateCompleted { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public DateTime DateModified { get; set; }
    public int appStatus { get; set; }


public Appraisal()
{
    //
    // TODO: Add constructor logic here
    //
}
}

[TableName("360AppraisalTemplate")]
[PrimaryKey("Id", autoIncrement = true)]
public class AppraisalTemplate
{
    public int CreatedBy { get; set; }
    public Guid TemplateId { get; set; }
    [PrimaryKeyColumn(AutoIncrement = true)]
    public int Id { get; set; }
    public string TemplateName { get; set; }
    public Boolean IsCategory { get; set; }
    public Boolean IsValue { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public int CatergoryId {get; set;}
    [NullSetting(NullSetting = NullSettings.Null)]
    public int ValueId { get; set; }

    /// <summary>
    /// Question will store category is IsCatergory is true
    /// </summary>
    public string question { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public string FormType { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public string WordingType { get; set; }
     public DateTime CreatedDate { get; set; }
    public int sortOrder { get; set; }
    public int CompetencyId { get; set; }

    public AppraisalTemplate()
    {
        }
}

///Individual Appraisals
[TableName("360AppraisalSetup")]
[PrimaryKey("Id", autoIncrement = true)]
public class AppraisalSetup
{
    [PrimaryKeyColumn(AutoIncrement = true)]
    public int Id { get; set; }
    public int MemberId { get; set; }
    public Guid AppraisalBatch { get; set; }
    public Guid templateId { get; set; }
    /// <summary>
    /// Id of person who you are completeing for
    /// </summary>
    public int UserId { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public int FinalScore { get; set; }
    public int QuestionsStatus {get; set;}
  
    public DateTime createdDate { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public DateTime CompletedDate { get; set; }
    [ResultColumn]
    public List<Appraisal> AppraisalAnswers { get; set; }
}

/// <summary>
/// Indivudal appraisal answers
/// </summary>
[TableName("360AppraisalAnswers")]
[PrimaryKey("Id", autoIncrement = true)]
public class AppraisalAnswers
{
    [PrimaryKeyColumn(AutoIncrement = true)]
    public int Id { get; set; }
    public int Answer { get; set; }

    [ResultColumn]
    public string Question { get; set; }
    [ResultColumn]
    public string questionType { get; set; }
    /// Id logged in person
    public int MemberId { get; set; }

    /// Id of person who you are completeing for
    public int UserId { get; set; }
    public Guid Guid { get; set; }
    public int sortOrder { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public string FormType { get; set; }
    public int QuestionTmeplateId { get; set; }
    public Boolean IsCategory { get; set; }
    public Boolean IsValue { get; set; }
    public int ValueId { get; set; }
    public int CompetencyId { get; set; }
    [ResultColumn]
    public string ValueName { get; set; }
    [ResultColumn]
    public string CompetencyName { get; set; }
    [ResultColumn]
    public List<AppraisalAnswers> QuestionList { get; set; }
    [ResultColumn]
    public int QuestionNumber { get; set; }

    public AppraisalAnswers()
    {
        Answer = 3;
        //
        // TODO: Add constructor logic here
        //
    }
}


