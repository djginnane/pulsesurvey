﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

[TableName("360SurveyValues")]
[PrimaryKey("Id", autoIncrement = true)]

    public class SurveyValues
{
    [PrimaryKeyColumn(AutoIncrement = true)]
    public int Id { get; set; }
    public string ValueName { get; set; }
    public int CreatedMemberId { get; set; }
    public DateTime CreatedDate { get; set; }
    public string ValueTitle { get; set; }
    public string GenericDescription { get; set; }

    public string DescriptionOne { get; set; }
    public string DescriptionTwo { get; set; }
    public string DescriptionThree { get; set; }
    public string DescriptionFour { get; set; }
    public string DescriptionFive { get; set; }

}

[TableName("360SurveyCompetencies")]
[PrimaryKey("Id", autoIncrement = true)]

public class SurveyCompetencies
{
    [PrimaryKeyColumn(AutoIncrement = true)]
    public int Id { get; set; }
    public string CompetencyName { get; set; }
    public int CreatedMemberId { get; set; }
    public DateTime CreatedDate { get; set; }
    public int ValueId { get; set; }
    public string GenericDescription { get; set; }
    public string DescriptionOne { get; set; }
    public string DescriptionTwo { get; set; }
    public string DescriptionThree { get; set; }
    public string DescriptionFour { get; set; }
    public string DescriptionFive { get; set; }

}


[TableName("360CompetencyAnalysis")]
[PrimaryKey("Id", autoIncrement = true)]
public class CompetencyAnalysis
{

    [PrimaryKeyColumn(AutoIncrement = true)]
    public int Id { get; set; }
    public string CompAnalysis { get; set; }
    public double ScoreRange { get; set; }

    public int CompId { get; set; }
    public string Section { get; set; }
 
   
}

[TableName("360ValueAnalysis")]
[PrimaryKey("Id", autoIncrement = true)]
public class ValueAnalysis
{

    [PrimaryKeyColumn(AutoIncrement = true)]
    public int Id { get; set; }
    public string ValueDescription { get; set; }
    public double ScoreRange { get; set; }

    public int ValueId { get; set; }
    public string Section { get; set; }
  
}