﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

/// <summary>
/// If companies require extra settings 
/// </summary>
[TableName("BtcUploadededCsv")]
[PrimaryKey("Id", autoIncrement = true)]
/// <summary>
/// Summary description for UploadedCsv
/// </summary>
public class UploadedCsv
{

    [PrimaryKeyColumn(AutoIncrement = true)]
    public int Id { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public string Filename { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public int hub { get; set; }
    public string Original_Document_Name { get; set; }
    public DateTime DateUPloaded { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public DateTime LastLoaded { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public int StaffCount { get; set; }
    public int UploadedBy { get; set; }

    public UploadedCsv()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}