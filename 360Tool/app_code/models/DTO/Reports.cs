﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


    public class Reports
    {
    public List<SpiderChart> SpiderResults { get; set; }
    public List<resultsTable> CompetencyResults { get; set; }
    public List<competencyTable>  CompetencySummary { get; set; }
}

    /// <summary>
    /// model for spider chart break down of types of reviewers, vs value scores
    /// </summary>
    public class SpiderChart
    {
        public string UserType { get; set; }
        public string ValueName { get; set; }
        public int ValueId { get; set; }
        public decimal AverageScore { get; set; }
        public int MemberId { get; set; }
    }

public class resultsTable
{
    public string CompetencyName { get; set; }
    public int CompetencyId { get; set; }
    public double Answer { get; set; }
    public string SurveyType { get; set; }
    public string CompetencyDescription { get; set; }
    public string CompetencyScoreDescription { get; set; }
    public List<resultsTable> CompetencyList { set; get; }
}

public class competencyTable
{
    public string CompetencyName { get; set; }
    public string ValueName { get; set; }
    public double TotalAverage { get; set; }
    public double ManagerAverage { get; set; }
    public double SubordinateAverage { get; set; }
    public double PierAverage { get; set; }
    public double SelfAssessment { get; set; }
}
