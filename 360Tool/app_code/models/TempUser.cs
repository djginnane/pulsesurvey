﻿using System.ComponentModel.DataAnnotations;
using System;
using Umbraco.Web.Models;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Web;
using Umbraco.Core.Models;
using System.Globalization;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;
/// <summary>
/// Summary description for UserTemp
/// </summary>
[TableName("BtcUserTemp")]
[PrimaryKey("Id", autoIncrement = true)]
public class UserTemp
{


    [Required]
    public string Name { set; get; }
    [Display(Name = "Staff Role")]
    public string Level { set; get; }
    [Display(Name = "Organisation")] 
    public string Organisation { set; get; }
    
    public string Country{ set; get; }
    
    

    public string Email { get; set; }
    [Display(Name = "Managers Email")]
    public string ManagerEmail { get; set; }
    [Display(Name = "Is a manager")]
    public bool IsManager { get; set; }
    [Display(Name = "Exclude from calculations")]
    public bool IsExcluded { get; set; }
    [PrimaryKeyColumn(AutoIncrement = true)]
    public int Id { set; get; }
 
    public int managerId { set; get; }


    public UserTemp()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}