﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;



[TableName("360AssignedSurvey")]
[PrimaryKey("Id", autoIncrement = true)]
public class AssignedSurveys
{
    [PrimaryKeyColumn(AutoIncrement = true)]
    public int Id { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public string Description { get; set; }
    public int ForMember { get; set; }
    public int CompletedBy { get; set; }
    public string CompletedByName { get; set; }
    public String SurveyType { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public int SurveyId { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public Guid TemplateId { get; set; }
    public DateTime DateAssigned { get; set; }
    public int status { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public Guid SurveyBatch { get; set; }

    [NullSetting(NullSetting = NullSettings.Null)]
    public int FinalScore { get; set; }

    [NullSetting(NullSetting = NullSettings.Null)]
    public DateTime DateGenerated { get; set; }
    [NullSetting(NullSetting = NullSettings.Null)]
    public DateTime CompletedDate { get; set; }
    [ResultColumn]
    public List<Appraisal> AppraisalAnswers { get; set; }
    [ResultColumn]
    public int Completed { get; set; }
    [ResultColumn]
    public int InCompleted { get; set; }
    public string MemberName { get; set; } 


}
public class UserDetails
{
    public string MemberName { get; set; }
    public int MemberId { get; set; }
    public string Email { get; set; }

    public string ManagerName { get; set; }
    public int ManagerId { get; set; }
    public List<AssignedSurveys> AssignedList { get; set; }
    public List<AssignedSurveys> SubBoardinateList { get; set; }
    public List<AssignedSurveys> PierList { get; set; }
    public List<AssignedSurveys> ManagerList { get; set; }
}

public class UserModels
{
    public UserDetails UserDet { get; set; }
    public AssignedSurveys UserAssigned { get; set; }
}

public class SurveyLists
{
    public List<AssignedSurveys> CompletedBy { get; set; }
    public AssignedSurveys SelfAssessment { get; set; }
    public int SurveyProgress { get; set; }
    public int AwaitingYourInput { get; set; }
    public int SurveyProgressTotal { get; set; }
    public int AwaitingTotal { get; set; }
}

public class SurveyCompleted
{
    public int ForMember { get; set; }
    public string MemberName { get; set; }
    public int total { get; set; }
    public int completed { get; set; }

}